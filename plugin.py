###
# Copyright (c) 2021, Glandos
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import itertools
import re
import requests
import arrow
from concurrent.futures import ThreadPoolExecutor, Future
from datetime import datetime
from lxml import html


from supybot import utils, plugins, ircutils, callbacks
from supybot.commands import *

try:
    from supybot.i18n import PluginInternationalization

    _ = PluginInternationalization("TwitterNoAPI")
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x

TWITTER_URL_PATTERN = re.compile(
    r"(https?://(?:mobile.)?twitter.com/\w+/status/(?P<id>\w+))"
)

MAIN_SCRIPT_PATTERN = re.compile(
    r"https://abs.twimg.com/responsive-web/(?:client-web(?:-legacy)?|web|web_legacy)/main\.\w+\.js$"
)
BEARER_TOKEN_FROM_SCRIPT_PATTERN = re.compile(r'"AAAAAAA.+?"')
GUEST_TOKEN_FROM_SCRIPT_PATTERN = re.compile(r"gt=(.*?);")

BEARER_TOKEN_PATTERN = re.compile(r'"Bearer .*?"')

# Map script URI with known bearer tokens
BEARER_TOKENS = {}

# Map bearer tokens with guest tokens
GUEST_TOKENS = {}


class TwitterNoAPI(callbacks.PluginRegexp):
    """Show tweet content without using a Twitter API key"""

    regexps = ["showTweet"]

    def get_bearer_token(self, script_uri, api_version=1):
        bearer_token = BEARER_TOKENS.get(script_uri, None)
        if bearer_token is not None:
            return bearer_token

        script = requests.get(script_uri)
        pattern = (
            BEARER_TOKEN_PATTERN
            if api_version == 1
            else BEARER_TOKEN_FROM_SCRIPT_PATTERN
        )
        match = pattern.search(script.text)
        if match:
            # Remove quotes
            bearer_token = match.group(0)[1:-1]
            BEARER_TOKENS[script_uri] = bearer_token

        return bearer_token

    def get_real_url(self, url):
        try:
            return requests.head(url, allow_redirects=True, headers={"user-agent": "curl/7.74.0"}).url
        except Exception:
            return url

    def extract_tweet(self, url):
        session = requests.Session()
        # Twitter needs a "real" browser.
        session.headers.update(
            {
                "user-agent": "Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
            }
        )
        page = session.get(url)
        tree = html.fromstring(page.content)

        links = tree.head.findall("link")
        link = next(
            (
                link
                for link in links
                if MAIN_SCRIPT_PATTERN.match(link.attrib.get("href") or "")
            ),
            None,
        )
        if link is None:
            raise RuntimeError("Unable to find main script")

        main_script_uri = link.attrib["href"]
        bearer_token = self.get_bearer_token(main_script_uri, 2)

        guest_token = session.cookies.get("gt")
        if guest_token is None:
            # Nothing in cookies, so let's extract from document
            scripts = tree.body.findall("script")
            script = next(
                (
                    s
                    for s in scripts
                    if s.text is not None and "document.cookie =" in s.text
                ),
                None,
            )
            if script is None:
                raise RuntimeError("Unable to find guest token")

            guest_token = GUEST_TOKEN_FROM_SCRIPT_PATTERN.search(script.text).group(1)

        tweet_id = TWITTER_URL_PATTERN.search(url).group("id")
        details = session.get(
            f"https://api.twitter.com/2/timeline/conversation/{tweet_id}.json",
            headers={
                "authorization": f"Bearer {bearer_token}",
                "x-guest-token": guest_token,
            },
            params={
                "include_profile_interstitial_type": "1",
                "include_blocking": "1",
                "include_blocked_by": "1",
                "include_followed_by": "1",
                "include_want_retweets": "1",
                "include_mute_edge": "1",
                "include_can_dm": "1",
                "include_can_media_tag": "1",
                "skip_status": "1",
                "cards_platform": "Web-12",
                "include_cards": "1",
                "include_ext_alt_text": "true",
                "include_reply_count": "1",
                "tweet_mode": "extended",
                "include_entities": "true",
                "include_user_entities": "true",
                "include_ext_media_color": "true",
                "include_ext_media_availability": "true",
                "send_error_codes": "true",
                "simple_quoted_tweet": "true",
                "count": "20",
                "ext": "mediaStats%2ChighlightedLabel%2CcameraMoment",
                "include_quote_count": "true",
            },
        )

        details = details.json()
        # print(details)

        tweet = details["globalObjects"]["tweets"][tweet_id]
        self.parse_tweet(tweet, details)
        replied_tweet = details["globalObjects"]["tweets"].get(
            tweet.get("in_reply_to_status_id_str")
        )
        quoted_tweet = details["globalObjects"]["tweets"].get(
            tweet.get("quoted_status_id_str")
        )
        if replied_tweet is not None:
            tweet["in_reply_to"] = self.parse_tweet(replied_tweet, details)

        if quoted_tweet is not None:
            tweet["quoted_status"] = self.parse_tweet(quoted_tweet, details)

        return tweet

    def parse_tweet(self, tweet, details):
        tweet["created_at"] = arrow.get(
            tweet["created_at"], "ddd MMM DD HH:mm:ss Z YYYY"
        )
        tweet["created_at"].humanized = tweet["created_at"].humanize(locale="fr")
        tweet["user"] = details["globalObjects"]["users"][tweet["user_id_str"]]
        tweet[
            "tweet_url"
        ] = f"https://twitter.com/{tweet['user']['screen_name']}/status/{tweet['id_str']}"

        for ((begin, end), urls) in self.get_media_by_indices(tweet):
            tweet["full_text"] = (
                tweet["full_text"][:begin] + " ".join(urls) + tweet["full_text"][end:]
            )

        # Remove newlines
        tweet["full_text"] = " – ".join(
            line for line in tweet["full_text"].split("\n") if line
        )
        return tweet

    def get_media_by_indices(self, tweet):
        all_media = {}
        with ThreadPoolExecutor() as executor:
            for media in itertools.chain(
                tweet["entities"].get("urls", []),
                tweet.get("extended_entities", {}).get("media", []),
            ):
                (begin, end) = media["indices"]
                real_url = media.get("media_url_https", media["expanded_url"])
                if media.get("type") == "video":
                    videos = [
                        v
                        for v in media["video_info"]["variants"]
                        if v.get("content_type", "").startswith("video")
                    ]
                    videos.sort(key=lambda video: video.get("bitrate"), reverse=True)
                    if videos:
                        real_url = videos[0]["url"]
                elif "url" in media:
                    # URL, try to follow redirections
                    real_url = executor.submit(self.get_real_url, real_url)

                all_media.setdefault((begin, end), []).append(real_url)

        for indices in sorted(all_media.keys(), reverse=True):
            yield (
                indices,
                [
                    (media.result() if isinstance(media, Future) else media)
                    for media in all_media[indices]
                ],
            )

    def format_tweet(self, tweet, show_url=False):
        when = ircutils.mircColor(
            tweet["created_at"].humanized,
            "dark grey",
            "light blue",
        )
        username = ircutils.bold(tweet["user"]["name"])

        url = f" @ {tweet['tweet_url']}" if show_url else ""
        return f"{when} - {username}: \"{tweet['full_text']}\" ({tweet['retweet_count']} RT){url}"

    def showTweet(self, irc: callbacks.IrcObjectProxy, msg, match):
        tweet = self.extract_tweet(match.group(1).replace("://mobile.", "://"))

        if not tweet:
            irc.reply("There is no tweet here", prefixNick=False)
            return

        irc.reply(self.format_tweet(tweet), prefixNick=False)

        quoted_tweet = tweet.get("quoted_status")
        if quoted_tweet is not None:
            irc.reply("Quoting: " + self.format_tweet(quoted_tweet, True), prefixNick=False)
        replied_tweet = tweet.get("in_reply_to")
        if replied_tweet is not None:
            irc.reply("In reply to: " + self.format_tweet(replied_tweet, True), prefixNick=False)

    showTweet = urlSnarfer(showTweet)
    showTweet.__doc__ = TWITTER_URL_PATTERN.pattern


Class = TwitterNoAPI


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
